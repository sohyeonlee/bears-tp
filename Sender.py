import sys
import getopt

import Checksum
import BasicSender
from sets import Set

'''
This is a skeleton sender class. Create a fantastic transport protocol here.
'''
class Sender(BasicSender.BasicSender):
    def __init__(self, dest, port, filename, debug=False, sackMode=False):
        super(Sender, self).__init__(dest, port, filename, debug)
        self.sndpkt = {} # a dictionary of sent packets
        # received out of order
        self.dupacks = 0 # number of duplicated acks
        self.currentack = None
        self.window = [] # window list size of 5
        self.seqnums = {} # enforce single instance of each seqno
        self.data_packets = self.update_packets(self.infile)
        
        if sackMode:
            self.receivedacks = Set()
            #print "i have a set", self.receivedacks #remove this line when you implement SACK

    # Main sending loop.
    def start(self):
        
        msg_type = None
        self.update_window()
        while len(self.window) != 0:
            #print "w size", len(self.window)
            for packet in self.window: # send everything in window ONLY when it hasn't been sent
                #print "in the window", self.seqnums[packet]
                if not self.sndpkt[packet]:
                    self.send(packet)
                    self.sndpkt[packet] = True
                    if self.debug:
                        print "Sender.py:sending %d" % (self.seqnums[packet])
            # after sending everything, wait for acks
            received = self.receive(0.5) # needs to be fixed, timeout not from when it's sent
            if not received:
                self.handle_timeout()
            else:
                #print "going to handle new ack"
                self.handle_new_ack(received) 
                    
    def handle_timeout(self):
       # print "i m here in timeout"
        if not sackMode:
            for packet in self.window:
                self.send(packet)
                if self.debug:
                    print "Sender.py:sending %d" % (self.seqnums[packet])
        if sackMode:
            for packet in self.window:
                seqno = self.seqnums[packet]
                #print self.receivedacks, "tahts waht we have received "
                if seqno not in self.receivedacks:
                    self.send(packet)

    def handle_new_ack(self, ack):

        msg_type, seqnum, data, checksum = self.split_packet(ack)
        if not sackMode:
            if not Checksum.validate_checksum(ack) or msg_type != 'ack':
                return False # drop the ACK
            else:
                if seqnum != self.currentack:
                    self.dupacks = 1
                    self.currentack = seqnum
                else:
                    self.dupacks += 1
                    if self.dupacks == 4:
                        #print "there is four now ???"
                        self.handle_dup_ack(seqnum) 
                        self.dupacks = 0# handled fast retransimit
                for packet in self.window:
                    thisseqnum = self.seqnums[packet]
                    if int(thisseqnum) < int(seqnum): # let dupacks update 
                        #print "packet poped", thisseqnum, "ack", seqnum
                        #print "poped", thisseqnum, "ack", seqnum
                        self.window.pop(0)
                        self.update_window() 


        if sackMode:
            #print "in sackmode"
            if not Checksum.validate_checksum(ack) or msg_type != 'sack':
                return False # drop the SACK
            else: 
                #self.seqno = int(self.seqno_str.split(';')[0]) - self.start_seqno_base
                #self.sack_str = self.seqno_str.split(';')[1]

                sacks = seqnum.split(";") 

                sack = sacks[0]
                received = sacks[1].split(",")
                #print "here is our sack" + sack
                for a in received:
                    if a != '':
                        #print "received", int(a)
                        self.receivedacks.add(int(a))
                if sack != self.currentack:
                    self.dupacks = 1
                    self.currentack = sack
                else:
                    self.dupacks += 1
                    if self.dupacks == 4:
                        self.handle_dup_ack(sack)
                        self.dupacks = 0 # handled fast retransimit
                for packet in self.window:
                    thisseqnum = self.seqnums[packet]
                    if int(thisseqnum) < int(sack): # let dupacks update 
                        #print "packet proceeded for sack", thisseqnum, seqnum
                        self.window.pop(0)
                        self.update_window() 




            
    def update_packets(self, infile): # make an iterator of packets
        seqno = 0
        msg_type = None
        curr_pack = infile.read(500)
        print(curr_pack)
        packet_lst = []
        while not msg_type == 'end':
            next_pack = infile.read(500)
            if seqno == 0:
                msg_type = 'start'
            elif next_pack == "":
                msg_type = 'end'
            else:
                msg_type = 'data'
            packet = self.make_packet(msg_type, seqno, curr_pack)
            self.seqnums[packet] = seqno # yan add it 
            self.sndpkt[packet] = False # initially setting them to false
            curr_pack = next_pack
            packet_lst.append(packet)
            seqno += 1
        return iter(packet_lst)
        
    def update_window(self): # update the current sender window
        while len(self.window) < 5:
            try:
                self.window.append(self.data_packets.next())
            except StopIteration:
                return

    def handle_dup_ack(self, ack): # might be minor errors
        #print " i m dup ack"
        #print "hi hi hi ", self.window[0]
        self.send(self.window[0])


    def log(self, msg):
        if self.debug:
            print msg


'''
This will be run if you run this script from the command line. You should not
change any of this; the grader may rely on the behavior here to test your
submission.
'''
if __name__ == "__main__":
    def usage():
        print "BEARS-TP Sender"
        print "-f FILE | --file=FILE The file to transfer; if empty reads from STDIN"
        print "-p PORT | --port=PORT The destination port, defaults to 33122"
        print "-a ADDRESS | --address=ADDRESS The receiver address or hostname, defaults to localhost"
        print "-d | --debug Print debug messages"
        print "-h | --help Print this usage message"
        print "-k | --sack Enable selective acknowledgement mode"

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                               "f:p:a:dk", ["file=", "port=", "address=", "debug=", "sack="])
    except:
        usage()
        exit()

    port = 33122
    dest = "localhost"
    filename = None
    debug = False
    sackMode = False

    for o,a in opts:
        if o in ("-f", "--file="):
            filename = a
        elif o in ("-p", "--port="):
            port = int(a)
        elif o in ("-a", "--address="):
            dest = a
        elif o in ("-d", "--debug="):
            debug = True
        elif o in ("-k", "--sack="):
            sackMode = True

    s = Sender(dest, port, filename, debug, sackMode)
    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        exit()