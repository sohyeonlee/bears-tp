import random
import time 
import BasicSender

from BasicTest import *

"""
This tests random packet drops. We randomly decide to drop about half of the
packets that go through the forwarder in either direction.

Note that to implement this we just needed to override the handle_packet()
method -- this gives you an example of how to extend the basic test case to
create your own.
"""
class SackTimeOutTest(BasicTest):
	def __init__(self, forwarder, input_file, sackMode = False):
		super(SackTimeOutTest,self).__init__(forwarder, input_file, sackMode = True )
		self.firsttime = 1
		self.forwarder = forwarder
	
	def handle_packet(self):
		for p in self.forwarder.in_queue:
			pieces = p.full_packet.split('|')
			seqno_str = pieces[1]
			msg_type = pieces[0]
			if msg_type == "data" and int(seqno_str) == 2  and self.firsttime <3 :
				#print "diddnt send "
				self.firsttime = self.firsttime+ 1
			else:
				#print "send " + seqno_str + "  " +  msg_type 
				self.forwarder.out_queue.append(p)
		# empty out the in_queue
		self.forwarder.in_queue = []
		
