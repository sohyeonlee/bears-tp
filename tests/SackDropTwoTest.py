import random
import time 
import BasicSender

from BasicTest import *

"""
This tests random packet drops. We randomly decide to drop about half of the
packets that go through the forwarder in either direction.

Note that to implement this we just needed to override the handle_packet()
method -- this gives you an example of how to extend the basic test case to
create your own.
"""
class SackDropTwoTest(BasicTest):
	def __init__(self, forwarder, input_file, sackMode = False):
		super(SackDropTwoTest,self).__init__(forwarder, input_file, sackMode = True )
		self.firsttime = 0 
		self.time = 0
		self.forwarder = forwarder
	
	def handle_packet(self):
		for p in self.forwarder.in_queue:
			pieces = p.full_packet.split('|')
			seqno_str = pieces[1]
			msg_type = pieces[0]
			if msg_type == "data" and int(seqno_str) < 6 and self.firsttime < 5 :
				#print "diddnt send ", p
				self.firsttime = self.firsttime + 1 

			# if msg_type == "data" and int(seqno_str) == 3  and self.first == True :
			# 	print "diddnt send "
			# 	self.first= False
			else:
				#print "send " + seqno_str + "  " +  msg_type
				self.forwarder.out_queue.append(p)
		# empty out the in_queue
		self.forwarder.in_queue = []